# Sentiment Analysis

![image](https://gitlab.com/PkSai/sentiment-analysis/-/raw/main/sen.jpg)

Sentiment analysis is the process of analyzing digital text to determine if the emotional tone of the message is positive, negative, or neutral.Businesses can avoid personal bias associated with human reviewers by using artificial intelligence (AI)–based sentiment analysis tools. As a result, companies get consistent and objective results when analyzing customers’ opinions.

# Preprocessing
During the preprocessing stage, sentiment analysis identifies key words to highlight the core message of the text.

- Tokenization breaks a sentence into several elements or tokens.
- Lemmatization converts words into their root form. For example, the root form of am is be.
- Stop-word removal filters out words that don't add meaningful value to the sentence. For example, with, for, at, and of are stop words. 

# Keyword analysis
NLP technologies further analyze the extracted keywords and give them a sentiment score. A sentiment score is a measurement scale that indicates the emotional element in the sentiment analysis system. It provides a relative perception of the emotion expressed in text for analytical purposes. For example, researchers use 10 to represent satisfaction and 0 for disappointment when analyzing customer reviews.
